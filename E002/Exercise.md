# Exercise 002 - Simple CLI calculator 2

Write a CLI programm which asks for the entry a mathematical operator to select one of the following 4 options:
* [+] Add
* [-] Subtract
* [*] Multiply
* [/] Divide

Query the user about the 2 necessary values to execute the calculations.

Print the equation and result.
The result should be accurate to 3 digits after the comma.

After printing the result let the user confirm the result by pressing [ENTER] and end the program with return code 0.

If any invalid input is given, then tell the user about it and abort the program with return code 1;

