# Exercise 004 - Simple CLI calculator 4

Ask the user to enter simple mathematical calculations, like:
* <value1> + <value2>
* <value1> - <value2>
* <value1> * <value2>
* <value1> / <value2>

Print the result 3 digits after the comma.

After printing the result ask the user for the next input.

If any invalid input is given, then ask the user to enter a valid command or value.

If "q" has been entered, then end the program with exit code 0.

