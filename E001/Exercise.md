# Exercise 001 - Simple CLI calculator 1

Write a CLI programm which asks for the entry of a digit from 1 to 4 to select one of the following 4 options:
* 1 - Add
* 2 - Subtract
* 3 - Multiply
* 4 - Divide

Query the user about the 2 necessary values to execute the calculations.

Print the equation and result.
The result should be accurate to 3 digits after the comma.

After printing the result let the user confirm the result by pressing [ENTER] and end the program with return code 0.

If any invalid input is given, then tell the user about it and abort the program with return code 1;

