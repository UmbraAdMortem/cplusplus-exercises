# Exercise 003 - Simple CLI calculator 3

Write a CLI programm which asks for the entry of on of the following commands:
* [+] Add
* [-] Subtract
* [*] Multiply
* [/] Divide
* [q] Quit

Query the user about the 2 necessary values to execute the calculations.

Print the equation and result.
The result should be accurate to 3 digits after the comma.

After printing the result ask the user for the next command.

If any invalid input is given, then ask the user to enter a valid command or value.

If "Quit" has been selected then end the program with exit code 0.

